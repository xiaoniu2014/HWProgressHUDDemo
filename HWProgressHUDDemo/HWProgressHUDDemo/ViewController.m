//
//  ViewController.m
//  HWProgressHUDDemo
//
//  Created by hongw on 16/3/10.
//  Copyright © 2016年 hongw. All rights reserved.
//


#define kBlueColor                              UIColorFromRGB(0x3097FD)

#pragma mark - COLOR
//--------------------------COLOR--------------------------
#define UIColorFromRGB(rgbValue) \
[UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0x00FF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0x0000FF))/255.0 \
alpha:1.0]

#define UIColorFromRGBA(rgbValue, alphaValue) \
[UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0x00FF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0x0000FF))/255.0 \
alpha:alphaValue]
//--------------------------COLOR--------------------------



#import "ViewController.h"
#import "HWProgressHUD.h"

@interface ViewController ()
{
    HWProgressHUD *_hud;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor yellowColor];
}

- (IBAction)buttonClick:(UIButton *)sender {
   [_hud dismiss];
}
- (IBAction)hidden:(UIBarButtonItem *)sender {
    [_hud dismiss];
}


- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    NSLog(@"%s",__FUNCTION__);
    _hud = [HWProgressHUD showToView:self.view image:@"background_loading_image" strokeColor:kBlueColor];
    _hud.maskType = HWProgressHUDMaskTypeClear;
}

@end
