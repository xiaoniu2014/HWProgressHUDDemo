//
//  AppDelegate.h
//  HWProgressHUDDemo
//
//  Created by hongw on 16/3/10.
//  Copyright © 2016年 hongw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

