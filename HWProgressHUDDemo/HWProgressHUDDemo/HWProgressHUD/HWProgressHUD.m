//
//  HWProgressHUD.m
//  HWProgressHUDDemo
//
//  Created by hongw on 16/3/10.
//  Copyright © 2016年 hongw. All rights reserved.
//

#import "HWProgressHUD.h"

#define DEGREES_TO_RADIANS(angle)   ((angle * M_PI) / 180.0)

//hud宽度
#define ANIMATION_WITH              150.0f
//hud半径
#define RADIUS                      (self.bounds.size.width / 2.0f)
//一个动画周期运动的角度
#define ALL_ANIMTAION_ANGEL         (360.0f + 120.0f)
//动画时间
#define ANIMATION_DURATION          10.0f

//第一阶段动画运动的角度
#define FIRST_ANGLE (45)
//第二阶段动画前半部分运动的角度
#define SECOND_ANGLE_STAR (120 - 45 + 7.2)
//第二阶段动画后半部分运动的角度
#define SECOND_ANGLE_END (300 - 120)
//第三阶段动画运动的角度
#define THIRD_ANGLE (180 + 14.4)

#define DURATION_WITH_ANGLE(angle) ((angle)/ALL_ANIMTAION_ANGEL * ANIMATION_DURATION)


@interface HWProgressHUD()
{
    NSInteger _flag;
}

@property (nonatomic, strong) CABasicAnimation *rotationAnimation;
@property (nonatomic, strong) CAAnimationGroup *animationGroup;
@property (strong, nonatomic) CABasicAnimation *strokeAnimatinStart;
@property (strong, nonatomic) CABasicAnimation *strokeAnimatinEnd;
@property (nonatomic,strong) CAShapeLayer *firstLayer;
@property (nonatomic,strong) CAShapeLayer *secondLayer;
@property (nonatomic,strong) CAShapeLayer *thirdLayer;

@property (nonatomic, strong) UIControl *overlayView;
@property (nonatomic,strong) UIImageView *imageView;
@property (nonatomic,strong) UIColor *strokeColor;
@property (nonatomic,copy) NSString *backgroundImageName;
@end

@implementation HWProgressHUD
//HWProgressHUD *shareHud;

+ (instancetype)showToView:(UIView *)superView image:(NSString *)imageName strokeColor:(UIColor *)color{
    HWProgressHUD *hud = [[self alloc] init];
//    shareHud = hud;
    if (imageName !=nil || imageName.length > 0) {
        
        hud.backgroundImageName = imageName;
    }
    if (color) {
        hud.strokeColor = color;
    }else {
        hud.strokeColor = [UIColor blueColor];
    }
    
    [hud updateViewHierachyWithView:superView];
    return hud;
}

- (void)updateViewHierachyWithView:(UIView *)superView {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(statusBarOrientationDidChange:)
                                                 name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
    if(!self.overlayView.superview) {
        // Default case: iterate over UIApplication windows
//        NSEnumerator *frontToBackWindows = [UIApplication.sharedApplication.windows reverseObjectEnumerator];
//        for (UIWindow *window in frontToBackWindows) {
//            BOOL windowOnMainScreen = window.screen == UIScreen.mainScreen;
//            BOOL windowIsVisible = !window.hidden && window.alpha > 0;
//            BOOL windowLevelNormal = window.windowLevel == UIWindowLevelNormal;
//            
//            if(windowOnMainScreen && windowIsVisible && windowLevelNormal) {
//                [window addSubview:self.overlayView];
//                break;
//            }
//        }
        
        [superView addSubview:self.overlayView];
    } else {
        [self.overlayView.superview bringSubviewToFront:self.overlayView];
    }
    
    // Add self to the overlay view
    if(!self.superview){
        [self.overlayView addSubview:self];
    }
    [self addSubview:self.imageView];
    self.center = CGPointMake(self.superview.bounds.size.width / 2.0, self.superview.bounds.size.height / 2.0);
    self.bounds = CGRectMake(0, 0, ANIMATION_WITH, ANIMATION_WITH);
    
    self.imageView.image = [UIImage imageNamed:self.backgroundImageName];
    NSInteger imageWidth = self.imageView.image.size.width;
    NSInteger imageHeight = self.imageView.image.size.height;
    self.imageView.frame = CGRectMake((ANIMATION_WITH - imageWidth)/2,
                                       (ANIMATION_WITH - imageHeight)/2,
                                       imageWidth,
                                       imageHeight);
    [self.layer addSublayer:self.firstLayer];
    [self starFirstAnimation];
}

- (UIImageView *)imageView{
    if (!_imageView) {
        _imageView = [[UIImageView alloc] init];
    }
    return _imageView;
}



- (void)statusBarOrientationDidChange:(NSNotification *)notification {
    self.center = CGPointMake(self.superview.bounds.size.width / 2.0, self.superview.bounds.size.height / 2.0);
    self.bounds = CGRectMake(0, 0, ANIMATION_WITH, ANIMATION_WITH);
}


- (UIControl*)overlayView {
    if(!_overlayView) {
        _overlayView = [[UIControl alloc] initWithFrame:[UIScreen mainScreen].bounds];
        _overlayView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _overlayView.backgroundColor = [UIColor clearColor];
        _overlayView.userInteractionEnabled = NO;
    }
    return _overlayView;
}

- (void)dismiss {
    [self removeFromSuperviewAndAnimationWithLayer:self.firstLayer];
    [self removeFromSuperviewAndAnimationWithLayer:self.secondLayer];
    [self removeFromSuperviewAndAnimationWithLayer:self.thirdLayer];
    
    [self.overlayView removeFromSuperview];
    [self removeFromSuperview];
    
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc removeObserver:self name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
}

- (void)removeFromSuperviewAndAnimationWithLayer:(CAShapeLayer *)layer{
    [layer removeAllAnimations];
    [layer removeFromSuperlayer];
}

#pragma mark - layer
- (CAShapeLayer *)firstLayer{
    if (!_firstLayer) {
        _firstLayer = [self layerWithBounds:self.bounds starAngle:0 strokeStart:0 strokeEnd:0.75];
    }
    return _firstLayer;
}

- (CAShapeLayer *)secondLayer{
    if (!_secondLayer) {
        CGFloat starAngle = DEGREES_TO_RADIANS((270 + 45 - 7.2));
        _secondLayer = [self layerWithBounds:self.bounds starAngle:starAngle strokeStart:0 strokeEnd:1];
    }
    return _secondLayer;
}

- (CAShapeLayer *)thirdLayer{
    if (!_thirdLayer) {
        CGFloat starAngle = DEGREES_TO_RADIANS((180 + 30 - 14.4));
        _thirdLayer = [self layerWithBounds:self.bounds starAngle:starAngle strokeStart:0 strokeEnd:0.04];
        
    }
    return _thirdLayer;
}

//基础方法
- (CAShapeLayer *)layerWithBounds:(CGRect)bounds starAngle:(CGFloat)star strokeStart:(CGFloat)start strokeEnd:(CGFloat)end{
    CAShapeLayer *shapeLayer = [[CAShapeLayer alloc] init];
    shapeLayer.bounds = bounds;
    CGFloat x = RADIUS;
    CGFloat y = RADIUS;
    shapeLayer.position = CGPointMake(x, y);
    
    UIBezierPath *path = [UIBezierPath bezierPathWithArcCenter:shapeLayer.position
                                                        radius:RADIUS
                                                    startAngle:star
                                                      endAngle:M_PI * 2 + star
                                                     clockwise:YES];
    
    shapeLayer.path = path.CGPath;
    shapeLayer.fillColor = [[UIColor clearColor] CGColor];
    shapeLayer.lineWidth = 2;
    shapeLayer.strokeColor = [self.strokeColor CGColor];
    shapeLayer.strokeStart = start;
    shapeLayer.strokeEnd = end;
    return shapeLayer;
}

- (void)starFirstAnimation{
    [self.firstLayer addAnimation:self.strokeAnimatinStart forKey:@"animationStar"];
    [self.firstLayer addAnimation:self.rotationAnimation forKey:@"firstRotation"];
}

- (void)starSecondAnimation{
    [self.secondLayer addAnimation:self.rotationAnimation forKey:@"secondRotation"];
    [self.secondLayer addAnimation:self.animationGroup forKey:@"animationGroup"];
}

- (void)starThirdAnimation{
    [self.thirdLayer addAnimation:self.strokeAnimatinEnd forKey:@"animationEnd"];
    [self.thirdLayer addAnimation:self.rotationAnimation forKey:@"thirdRotation"];
}



#pragma -mark animation
- (CABasicAnimation *)rotationAnimation{
    if (!_rotationAnimation) {
        _rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
        _rotationAnimation.toValue = [NSNumber numberWithFloat:2 * M_PI];
        _rotationAnimation.duration = ANIMATION_DURATION * (360.0 / ALL_ANIMTAION_ANGEL);
        _rotationAnimation.repeatCount = MAXFLOAT;
        _rotationAnimation.removedOnCompletion = NO;
    }
    return _rotationAnimation;
}

- (CABasicAnimation *)strokeAnimatinStart {
    if (!_strokeAnimatinStart) {
        _strokeAnimatinStart = [CABasicAnimation animationWithKeyPath:@"strokeStart"];
        _strokeAnimatinStart.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        _strokeAnimatinStart.duration = DURATION_WITH_ANGLE(FIRST_ANGLE);
        _strokeAnimatinStart.fromValue = @(30 / 360.0f);
        _strokeAnimatinStart.toValue = @(0.73);
        
        _strokeAnimatinStart.delegate = self;
        _strokeAnimatinStart.removedOnCompletion = NO;
        _strokeAnimatinStart.fillMode = kCAFillModeForwards;
    }
    return _strokeAnimatinStart;
}

- (CABasicAnimation *)strokeAnimatinEnd {
    if (!_strokeAnimatinEnd) {
        _strokeAnimatinEnd = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
        _strokeAnimatinEnd.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        _strokeAnimatinEnd.duration = DURATION_WITH_ANGLE(THIRD_ANGLE);
        _strokeAnimatinEnd.fromValue = @(0.04);
        _strokeAnimatinEnd.toValue = @(240/360.0f);
        
        _strokeAnimatinEnd.delegate = self;
        _strokeAnimatinEnd.removedOnCompletion = NO;
        _strokeAnimatinEnd.fillMode = kCAFillModeForwards;
    }
    return _strokeAnimatinEnd;
}

- (CAAnimationGroup *)animationGroup {
    if (!_animationGroup) {
        
        // Stroke Tail
        CABasicAnimation *tailAnimation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
        tailAnimation.fromValue = @0.02;
        tailAnimation.toValue = @1;
        tailAnimation.duration = DURATION_WITH_ANGLE(SECOND_ANGLE_STAR);
        tailAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        
        
        // Stroke Head
        CABasicAnimation *headAnimation = [CABasicAnimation animationWithKeyPath:@"strokeStart"];
        headAnimation.beginTime = DURATION_WITH_ANGLE(SECOND_ANGLE_STAR);
        headAnimation.fromValue = @0;
        headAnimation.toValue = @0.96;
        headAnimation.duration = DURATION_WITH_ANGLE(SECOND_ANGLE_END);
        headAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        
        
        // Stroke Line Group
        _animationGroup = [CAAnimationGroup animation];
        _animationGroup.duration = DURATION_WITH_ANGLE(SECOND_ANGLE_STAR + SECOND_ANGLE_END);
        _animationGroup.delegate = self;
        _animationGroup.animations = @[tailAnimation,headAnimation];
        _animationGroup.removedOnCompletion = NO;
        _animationGroup.fillMode = kCAFillModeForwards;
    }
    return _animationGroup;
}

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag{
    if (_flag == 0) {
        [self.layer addSublayer:self.secondLayer];
        [self starSecondAnimation];
        [self removeFromSuperviewAndAnimationWithLayer:self.firstLayer];
        _flag = 1;
    }else if(_flag == 1){
        [self.layer addSublayer:self.thirdLayer];
        [self starThirdAnimation];
        [self removeFromSuperviewAndAnimationWithLayer:self.secondLayer];
        _flag = 2;
    }else if (_flag == 2){
        [self.layer addSublayer:self.firstLayer];
        [self starFirstAnimation];
        [self removeFromSuperviewAndAnimationWithLayer:self.thirdLayer];
        _flag = 0;
    }
}


- (void)setMaskType:(HWProgressHUDMaskType)maskType{
    if (_maskType != maskType) {
        _maskType = maskType;
        BOOL userInterfaceEnable;
        switch (maskType) {
            case HWProgressHUDMaskTypeNone:
                userInterfaceEnable = NO;
                break;
            case HWProgressHUDMaskTypeClear:
                userInterfaceEnable = YES;
                break;
                
            default:
                break;
        }
        self.overlayView.userInteractionEnabled = userInterfaceEnable;
    }
}


@end
