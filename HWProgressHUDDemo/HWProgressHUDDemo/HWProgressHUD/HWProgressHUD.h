//
//  HWProgressHUD.h
//  HWProgressHUDDemo
//
//  Created by hongw on 16/3/10.
//  Copyright © 2016年 hongw. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface HWProgressHUD : UIView

typedef NS_ENUM(NSUInteger, HWProgressHUDMaskType) {
    HWProgressHUDMaskTypeNone,  //默认没有蒙版，背景可以点中
    HWProgressHUDMaskTypeClear  //透明蒙版，背景不可点中
};

@property (nonatomic,assign)  HWProgressHUDMaskType  maskType;

+ (instancetype)showToView:(UIView *)superView image:(NSString *)imageName strokeColor:(UIColor *)color;
- (void)dismiss;

@end
