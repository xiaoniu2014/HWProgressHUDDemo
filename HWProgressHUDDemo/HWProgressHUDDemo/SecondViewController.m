//
//  SecondViewController.m
//  HWProgressHUDDemo
//
//  Created by hongw on 16/3/13.
//  Copyright © 2016年 hongw. All rights reserved.
//

#import "SecondViewController.h"
#import  "HWProgressHUD/HWProgressHUD.h"

@interface SecondViewController ()
{
    HWProgressHUD *_hud;
}

@end

@implementation SecondViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
- (IBAction)click:(UIButton *)sender {
    [_hud dismiss];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    NSLog(@"%s",__FUNCTION__);
    
//    [HWProgressHUD showWithImage:@"background_loading_image" strokeColor:[UIColor redColor]];
    _hud = [HWProgressHUD showToView:self.view image:@"background_loading_image" strokeColor:[UIColor blueColor]];
    
}

@end
